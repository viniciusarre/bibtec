import tornado.web
from tornado.options import define, options
from dev.app.Operations.SelectLibros import SelectLibros

define("port", default=8001, help="run on the given port", type=int)




if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[
    (r"/select",SelectLibros)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
