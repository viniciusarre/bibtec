from dev.app.Model.DAO import DAO

class Libro ():
    def select(self):
        d = DAO()
        try:
            d.connect()
            # sql = "SELECT titulo, cantidad, CONCAT(nombre,CONCAT(' ',apellido)) as autor FROM [BIBTEC].dbo.Libro l JOIN [BIBTEC].dbo.Autores a ON a.idLibros = l.idLibro LEFT JOIN [BIBTEC].dbo.Autor au ON au.idAutor=a.idAutor"
            sql = "SELECT titulo, CONCAT(nombre,CONCAT(' ',apellido)) as autor, " \
                  "descripcionArea Area, g.tipo genero, COUNT(IdReservaLibrosCabecera) - cantidadCopias   AS cantDisponible " \
                  "FROM [BIBTEC].dbo.Libro l LEFT JOIN [BIBTEC].dbo.Autores a ON a.idLibros = l.idLibro " \
                  "LEFT JOIN [BIBTEC].dbo.Autor au ON au.idAutor=a.idAutor " \
                  "LEFT JOIN subArea sa on sa.idSubArea = l.idSubArea " \
                  "LEFT JOIN area ar on ar.idArea = sa.idArea " \
                  "LEFT JOIN subGeneroLibro sgl on sgl.idLibros = l.idLibro " \
                  "LEFT JOIN subGenero sg on sg.idGenero = sgl.idSubGenero " \
                  "LEFT JOIN genero g on sg.idGenero = g.idGenero " \
                  "LEFT JOIN Copias c on c.idLibros = l.idLibro " \
                  "LEFT JOIN reservaLibroDetalle rld on rld.idCopias = c.idCopias " \
                  "LEFT JOIN ReservaLibrosCabecera rlc on rlc.IdReservaLibrosCabecera=rld.idReservaLibroCabecera " \
                  "GROUP BY titulo, CONCAT(nombre,CONCAT(' ',apellido)), cantidadCopias,descripcionArea, g.tipo"
            print(sql)
            d.cursor.execute(sql)
            return d.cursor.fetchall()
        except Exception as e:
            print('error no select ' + str(e))
        finally:
            d.close()
