import pypyodbc
class DAO:
    def __init__(self):
        self.cursor = None
        self.connection = None
    def connect(self):
        try:
            self.connection = pypyodbc.connect \
                    ('Driver={SQL Server};'
                      'Server=localhost;'
                      'Database=BIBTEC;'
                      'Uid=python;Pwd=123456;'
                        'Trusted_Connection=yes;'
                     )
            self.cursor = self.connection.cursor()
        except Exception as error:
            print("Error connecting to the Database", error)

    def close(self):
        self.cursor.close()
        self.connection.close()

