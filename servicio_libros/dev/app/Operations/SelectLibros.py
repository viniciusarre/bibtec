import tornado.web
from dev.app.Model.Libro import Libro
import tornado.escape

class SelectLibros (tornado.web.RequestHandler):
    def get(self):
        l = Libro()
        r = l.select()
        l = []
        for i in r:
            titulo = str(i[0])
            cantidad = str(i[1])
            autor = str(i[2])
            l.append({'Titulo':titulo.replace('\u00f1','ñ'),"Cantidad":cantidad.replace('\u00f1','ñ'),"autor":autor.replace('\u00f1','ñ')})
        self.write({"sucess":True, "dados":l})

    def set_default_headers(self):
        print("setting headers!!!")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

