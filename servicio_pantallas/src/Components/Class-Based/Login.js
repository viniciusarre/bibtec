import React,{Component} from 'react';
import {FormGroup,Button, ControlLabel, FormControl} from 'react-bootstrap';
import {LoginUser} from "../../Calls";
import {Box} from "../Stateless/Messages";
import ListarLibros from "./ListarLibros";

export default class Login extends Component {
    constructor() {
        super();
        this.state = {error: false, datos: {}, success:false, cargando: false, errorTxt:'', successTxt:'', loggedIn:true}
    };


    setLogin = () => {
        const e = !((this.state.datos.correo === "" || undefined || null) || (this.state.datos.clave === ""||undefined || null));
        if (e) {
            const r = LoginUser(this.state.datos);
            r.then((response)=>{
                if (response.data.emailError) this.setState({success:false,error:true,errorTxt:'Correo invalido '});
                else if (response.data.LoggedIn) this.setState({display:false, error:false,success:true,successTxt:"Bienvenido! cargando...."});
                else this.setState({error:true,errorTxt:'Contraseña incorrecta '});

            }).catch((error)=>{
                this.setState({error:true,errorTxt:'Connection error ' + error})
            });
        }else{
            this.setState({error:true});
            this.setState({errorTxt:'Campos de correo y/0 contraseña vacios!'})
        }
    };


    render() {
        return this.state.success?<ListarLibros email={this.state.datos.correo}/>
            :(<div> <FormGroup style={{marginLeft:'10%',marginRight:'10%'}}>
                {(this.state.error || this.state.success) &&
                <Box
                    type = {(this.state.success===true)?'success':'danger'}
                    msg={(this.state.error) === true?this.state.errorTxt:this.state.successTxt
                    }
                />
                }
                <ControlLabel>Correo</ControlLabel>
                <FormControl
                    name="correo"
                    type="email"
                    label="correo"
                    placeholder="correo"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                />
                <ControlLabel>Contraseña</ControlLabel>
                <FormControl
                    name="clave"
                    type="password"
                    label="Clave"
                    placeholder="Clave"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}

                />

                <Button style={{marginTop:'5%', marginBottom:'5%',marginLeft:0, marginRight:0}} bsStyle={"success"} onClick={()=>this.setLogin()}>Enviar</Button>

           </FormGroup></div>);
    }
}
