import React,{Component} from 'react';
import {Button, FormControl, FormGroup, ControlLabel } from 'react-bootstrap';
import {Box} from "../Stateless/Messages";
import {registerUser} from "../../Calls";

export default class Signup extends Component {
    constructor() {
        super();
        this.state = {error: false, errorTxt:'', datos:{}, success:false, campo:'',pval:''};

    };

    setLogin = () => {
        const e = !((this.state.datos.correo === "" || undefined || null) || (this.state.datos.clave === ""||undefined || null));
        if (e) {
            console.log(this.state.datos);
            let r = registerUser(this.state.datos);
            r.then((response)=>{
                if (response.data.existe) this.setState({success:false,error:true,errorTxt:'Correo ya cadastrado '});
                else if (response.data.success) this.setState({error:false,success:true,successTxt:'Datos registrados'});
                else this.setState({error:true,errorTxt:'Error al insertar datos '});

            }).catch((error)=>{
               this.setState({error:true,errorTxt:'Connection error ' + error})
            });
        }else{
            this.setState({error:true});
            this.setState({errorTxt:'Campos de correo y/0 contraseña vacios!'})
        }
    };
    render() {
        console.log(this.state.datos);
        return (
            <FormGroup style={{marginLeft:'10%',marginRight:'10%'}}>
                {(this.state.error || this.state.success) &&
                <Box
                    type = {(this.state.success===true)?'success':'danger'}
                    msg={(this.state.error) === true?this.state.errorTxt:this.state.successTxt
                     }
                />
                }
                    <ControlLabel>Nombre</ControlLabel>
                <FormControl
                    id="nombre"
                    name="nombre"
                    type="text"
                    label="Nombre"
                    placeholder="Nombre"
                    autoFocus
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                />
                    <ControlLabel>Apellido</ControlLabel>
                <FormControl
                    name="apellido"
                    type="text"
                    label="Apellido"
                    placeholder="apellido"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                    //onBlur = {()=>this.setState({data:{...this.state.data, dt}})}
                />
                    <ControlLabel>Correo</ControlLabel>
                <FormControl
                    name="correo"
                    type="email"
                    label="correo"
                    placeholder="correo"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                   //onBlur = {()=>this.setState({data:{...this.state.data, dt}})}
                />
                    <ControlLabel>Rut</ControlLabel>
                <FormControl
                    name="rut"
                    type="text"
                    label="rut"
                    placeholder="rut"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                   // onBlur = {()=>this.setState({data:{...this.state.data, dt}})}
                />   <ControlLabel>Contraseña</ControlLabel>
                    <FormControl
                    name="clave"
                    type="password"
                    label="Clave"
                    placeholder="Clave"
                    onChange = {(e)=> this.setState({datos: {...this.state.datos, [e.target.name]:e.target.value}})}
                    //onBlur = {()=>this.setState({data:{...dt, ...this.state.data}})}
                />

                    <Button style={{marginTop:'5%', marginBottom:'5%',marginLeft:0, marginRight:0}} bsStyle={"success"} onClick={()=>this.setLogin()}>Enviar</Button>


            </FormGroup>


        );
    }
}
