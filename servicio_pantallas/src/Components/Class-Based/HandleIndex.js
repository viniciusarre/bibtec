import React, {Component } from 'react'
import Login from "./Login";
import Signup from "./Signup";

export default class HandleIndex extends Component{
    constructor(){
        super();
        this.state = {login:false, loggedin:false}
    }
    render(){
        return(
            <div>
                {(this.state.login) &&
                        <div><a href={'#'} id={"link"} onClick={()=>this.setState({login:false})}>volver</a>
                            <Login/>
                        </div>}
                {!this.state.login && (
                    <div>
                        <a href={'#'} onClick={()=>this.setState({login:true})}>Ya tengo cuenta</a>
                        <Signup/>
                    </div>)}
            </div>

        );
    }
};