import React,{Component} from 'react';
import {FormGroup,Button, ControlLabel, FormControl} from 'react-bootstrap';
import {LoginUser} from "../../Calls";
import {Box} from "../Stateless/Messages";

export default class ListarLibros extends Component {

    render() {
        let {email} = this.props;
        return (
            <div>Metodo: Listar... Usuario = {email}</div>
        );
    }

    componentWillMount(){
        document.getElementById('link').style.display='none';
    }
}
