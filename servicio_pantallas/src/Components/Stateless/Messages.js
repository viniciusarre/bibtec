import React from 'react';

export const Box = ({type, msg}) =>{
    return (
        <blockquote className={'panel panel-'+type}>
            {msg}
        </blockquote>

    );
};