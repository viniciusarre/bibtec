import React from 'react';
import {Navbar} from 'react-bootstrap';
import logo  from '../../img/logo.jpg' ;

export const Header = () =>{
    return (
    <Navbar bsStyle={"inverse"}>
        <img className={'center-block img-responsive'}  style={{maxHeight:150}} src={logo} alt={'logo'}/>
    </Navbar>
    );
};
