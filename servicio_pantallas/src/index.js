import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Route, Switch} from 'react-router';
import './index.css';
import {Header} from './Components/Stateless/Header';
import Footer from './Components/Stateless/Footer';
import ListarLibros from "./Components/Class-Based/ListarLibros";
import HandleIndex from "./Components/Class-Based/HandleIndex";

ReactDOM.render(
    <div>
        <Header/>
        <BrowserRouter>

            <Switch>
                <Route exact path="/" component={HandleIndex}/>
                <Route exact path="/listar" component={ListarLibros}/>
            </Switch>

        </BrowserRouter>
        <Footer/>
    </div>, document.getElementById('root'));