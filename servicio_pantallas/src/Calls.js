import axios from 'axios';


export const registerUser =  (datos) =>{
    let hash = toHash(datos.clave);
    return axios({method:'post', url: 'http://localhost:8000/signup/',params: {
        correo:datos.correo,
        clave:hash,
        nombre:datos.nombre,
        apellido:datos.apellido,
        rut:datos.rut,
        tipo:2}}
    );

};

export const LoginUser = (datos) =>{
    let hash = toHash(datos.clave);
    return axios({method:'post', url: 'http://localhost:8000/login/',params: {
            email:datos.correo,
            password:hash
        }});

};


const toHash = (clave)=>{
    let sha1 = require('js-sha1');
    return sha1(clave);
};
