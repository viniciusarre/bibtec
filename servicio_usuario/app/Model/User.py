from app.Model.DAO import DAO
from app.util.Mail import newUserMail
class User:

 #   def validateEmail(self):
   #     self.d

    def select(self):
        d = DAO()
        try:
            d.connect()
            sql = 'SELECT * FROM [BIBTEC].dbo.Usuario';
            print(sql)
            d.cursor.execute(sql)
            return d.cursor.fetchall()
        except Exception as e:
            print('error no select '+ e)
        finally:
            d.close()

    def validateEmail(self, email):
        d = DAO()
        try:
            d.connect()
            sql = 'SELECT * FROM [BIBTEC].dbo.Usuario WHERE EMAIL = ?'
            Value = [email]
            r = d.cursor.execute(sql, Value)
            return r.rowcount is -1
        except Exception as e:
            print(e)
        finally:
            d.close()
         
    def validateLogin(self,email, passwd):
        d = DAO()
        try:
            d.connect()
            sql = 'SELECT * FROM [BIBTEC].dbo.Usuario WHERE EMAIL = ? AND CLAVE = ?'
            Value = [email, passwd]
            r = d.cursor.execute(sql, Value)
            return r.rowcount is -1
        except Exception as e: print(e)
        finally: d.close()

    def insert(self,correo,clave,nombre,apellido,rut,idTipo):
        d = DAO()
        try:
            d.connect()
            sql = 'INSERT INTO [BIBTEC].dbo.Usuario (EMAIL, CLAVE, NOMBRE, APELLIDO, RUT, IDTIPOUSUARIO) VALUES (?,?,?,?,?,?)'
            Values = [correo,clave,nombre,apellido,rut,idTipo]
            d.cursor.execute(sql, Values)
            d.connection.commit()
            newUserMail(correo)
            return {'success': True}
        except Exception as e:
            print(e)

            return {'success':False}