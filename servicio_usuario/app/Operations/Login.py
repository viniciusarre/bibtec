from app.Model.User import User

import tornado.web

class Login(tornado.web.RequestHandler):
    def post(self):
        email = self.get_argument('email')
        passwd = self.get_argument('password')
        print(email)
        print(passwd)
        u = User()
        if u.validateEmail(email):
            if u.validateLogin(email, passwd):
                self.write({"LoggedIn":True})
            else:
                self.write({"LoggedIn": False, "passwordError":True})
        else:
            self.write({"LoggedIn":False,"emailError":True})


    def set_default_headers(self):
        print("setting headers!!!")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

