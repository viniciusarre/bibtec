from app.Model.User import User

import tornado.web


class SignUp(tornado.web.RequestHandler):
    def post(self):
        correo = self.get_argument('correo')
        clave = self.get_argument('clave')
        nombre = self.get_argument('nombre')
        apellido = self.get_argument('apellido')
        rut = self.get_argument('rut')
        idTipo = self.get_argument('tipo')
        print(correo)
        u = User()
        if not u.validateEmail(correo): self.write(u.insert(correo,clave,nombre,apellido,rut,idTipo))
        else: self.write({'existe':True })
    get = options = post
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')


