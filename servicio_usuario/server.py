import tornado.web
from tornado.options import define, options
from app.Operations.Login import Login
from app.Operations.Signup import SignUp
from app.Operations.Teste import Teste

define("port", default=8000, help="run on the given port", type=int)




if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[
    (r"/login/", Login), (r"/signup/",SignUp)])
    #print('Iniciando')
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    #print('corriendo en el puerto ' + str(options.port))
    tornado.ioloop.IOLoop.instance().start()
    #print('corriendo en el puerto 8000')
